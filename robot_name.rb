module BookKeeping
  VERSION = 2
end

class Robot
  attr_reader :name
  @@robot_names = []
  def initialize
    while @@robot_names.include?(@name) do
      @name = ('A'..'Z').to_a.shuffle[0,2].join
      @name.concat(('0'..'9').to_a.shuffle[0,3].join)
    end
    @@robot_names.push(@name)
    @name
  end
  
  def reset
    initialize
  end
end